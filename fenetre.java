
import java.awt.*;
import java.applet.Applet;
import javax.swing.*;
import java.awt.event.ActionListener; 
import java.awt.event.ActionEvent;   


                                 
public class fenetre extends JFrame {

    compteur val = new compteur();
    JLabel valeur_tot = new JLabel();
    JLabel int_min = new JLabel();
    JLabel int_max = new JLabel();

    public fenetre(){

	JFrame w = new JFrame();
	JMenuBar barreMenus = new JMenuBar() ;
	
	JMenu menu1 = new JMenu("Fichier") ;
	JMenuItem quitter = new JMenuItem("Quitter");
	barreMenus.add(menu1) ;
	menu1.add(quitter);
	quitter.addActionListener(new quitterListener());


	JMenu menu2 = new JMenu("Modifier");
	JMenuItem incremente = new JMenuItem("+1");
	JMenuItem decremente = new JMenuItem("-1");
	menu2.add(incremente);
	menu2.add(decremente);
	incremente.addActionListener(new incrListener());
	decremente.addActionListener(new decrListener());
	
	JMenuItem intervalle_maximum = new JMenuItem("Intervalle Maximum");
	JMenuItem intervalle_minimum = new JMenuItem("Intervalle Minimum");
	JMenu menu3 = new JMenu("Intervalle");
	menu3.add(intervalle_maximum);
	menu3.add(intervalle_minimum);
	intervalle_maximum.addActionListener(new maximumListener());
        intervalle_minimum.addActionListener(new minimumListener());

	JPanel panel = new JPanel();
	BorderLayout compteur = new BorderLayout();
	panel.setLayout(compteur);

	JButton decr = new JButton("-1");
	JButton incr = new JButton("+1"); 
	decr.addActionListener(new decrListener());
	incr.addActionListener(new incrListener());

	JLabel label = new JLabel();
	JPanel panel2 = new JPanel();
	panel2.add("West",decr);
	panel2.add("East",incr);
	panel.add("South", panel2);

	
	panel.add("West",int_min);
	int_min.setText("" +val.get_intervalle_min()); 
	panel.add("East",int_max);
	int_max.setText("" +val.get_intervalle_max()); 

        
	valeur_tot.setText("" + this.val.get_valeur());
	panel.add("Center", valeur_tot);

       	barreMenus.add(menu2) ;
       	barreMenus.add(menu3) ;

        w.setJMenuBar(barreMenus) ;
	w.setContentPane(panel);   
	w.setTitle("Compteur");
	w.setBounds(100,100,300,200);
	w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	w.setVisible(true);
	pack();
    }
   
    public class incrListener implements ActionListener {

	public void actionPerformed(ActionEvent e) {
	    
	    int x=val.get_valeur();
	    int y=val.get_intervalle_max();
	    x++;
	    if (x > y) {
	    }
	    else {
	    val.set_valeur(x);
	    valeur_tot.setText("" + val.get_valeur());
	    }
	}
    }



    public class decrListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	
	    int x=val.get_valeur();
	    int z=val.get_intervalle_min();
	    x--;
	    if (x < z){

	    }
	    else {
	    val.set_valeur(x);
	    valeur_tot.setText("" + val.get_valeur());
	    }
	    
	}
    
    }

    public class quitterListener implements ActionListener {

	public void actionPerformed(ActionEvent e){
	    System.exit(0);
	 
	}
    }
  public class maximumListener implements ActionListener {

	public void actionPerformed(ActionEvent e){
	   double  reponse_max = Double.parseDouble(JOptionPane.showInputDialog(null, "Entrez l'intervalle maximum")) ;
	   int reponse = (int) reponse_max;
	    val.set_intervalle_max(reponse);
	     int_max.setText("" + val.get_intervalle_max());
	}
  }
	 public class minimumListener implements ActionListener {

	public void actionPerformed(ActionEvent e){
	   double  reponse_min = Double.parseDouble(JOptionPane.showInputDialog(null, "Entrez l'intervalle minimum")) ;
	   int reponse = (int) reponse_min;
	    val.set_intervalle_min(reponse);
	     int_min.setText("" + val.get_intervalle_min());
	}
    }





	public static void main(String[] a) {
	   
	    new fenetre();
	  
	}}
    
