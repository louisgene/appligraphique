
public class compteur {
    public int valeur;
    public int intervalle_min;
    public int intervalle_max;
    
    int get_valeur() {
	return this.valeur;
    }
    
    int get_intervalle_min() {
	return this.intervalle_min;
    }
    
    int get_intervalle_max() {
	return this.intervalle_max;
    }
    
    void set_valeur( int v) {
	this.valeur=v;
    }
    void set_intervalle_min(int v) {
	this.intervalle_min=v;
    }
    void set_intervalle_max(int v) {
	this.intervalle_max=v;
    }
}
